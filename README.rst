Linux Test Project for Morello
==============================

Morello LTP, project enrolled as a fork of the upstream `Linux Test Project
<http://linux-test-project.github.io/>`_, aims at enabling LTP with architectural
capabilities based on `CHERI <https://www.cl.cam.ac.uk/research/security/ctsrd/cheri/>`_
model (and as provided by `Morello <https://www.arm.com/architecture/cpu/morello>`_
architecture). Independently developed from its original project, Morello LTP
places its focus on verifying and validating capabilities support within the
`(Morello) Linux kernel <https://git.morello-project.org/morello/kernel/linux>`_
[#]_ and, at this point, targets *kernel syscalls testcases* only. Due to
external limitations only static builds are supported.

Discussions, contributions and reporting issues conveyed on the mailing list:
https://op-lists.linaro.org/mailman3/lists/linux-morello-ltp.op-lists.linaro.org

.. [#] Mainline-based fork of the Linux kernel for Morello supporting
       pure-capability kernel-user ABI (PCuABI)
       (see `Morello Linux ABI specifications <https://git.morello-project.org/morello/kernel/linux/-/wikis/home>`_)

Building & running relevant tests (purecap and plain aarch64 mode)
------------------------------------------------------------------

Dependencies/requirements: (additional to what's required to build base LTP):

- Morello LLVM toolchain (version >= 1.4)

  Sources: https://git.morello-project.org/morello/llvm-project

  Binary releases: https://git.morello-project.org/morello/llvm-project-releases

    - **morello/baremetal-release-1.x*** + **morello/linux-aarch64-release-1.x*** for Morello
      purecap crt objects


- Musl libc for Morello (version >= 1.4): https://git.morello-project.org/morello/musl-libc

- Kernel uAPI header files: https://git.morello-project.org/morello/morello-linux-headers

\* All based on the same release/tag version.

Building Musl:
--------------

Assumptions:

.. code-block::

   clang                - Morello Clang (see Dependencies) configured for current environment


.. code-block::

   TRIPLE               - Target architecture:
                                - morello+c64 (purecap): aarch64-linux-musl_purecap
                                - plain aarch64:         aarch64-linux-gnu

   MORELLO_SUPPORT      - Enabling support for Morello
                                - morello+c64 (purecap): --enable-morello
                                - plain aarch64:         --disable-morello

.. code-block::


    CC=clang ./configure \
    --disable-shared \
    ${MORELLO_SUPPORT} \
    --disable-libshim \
    --target=${TRIPLE} \
    --prefix=${INSTALL_PATH}


.. code-block::

    make
    make install


For more details see `Musl libc for Morello README <https://git.morello-project.org/morello/musl-libc/-/blob/morello/master/README.rst>`_.

Building LTP syscall testcases:
-------------------------------
Assumptions:

.. code-block::

    clang            - Morello Clang (see Dependencies) configured for current environment
    TRIPLE           - target architecture:
                                - morello+c64 (purecap): aarch64-linux-musl_purecap
                                - plain aarch64:         aarch64-linux-gnu
                       (needs to match *TRIPLE* specified for Musl build)
    KHDR_DIR         - kernel uAPI headers (see Dependencies or alternatively headers
                       installed for Morello Linux kernel through make headers_install)
    MUSL             - installed Musl libc for Morello (see section above: `Building Musl`);
                       this will serve as a location for --sysroot option
                       (note this must match chosen configuration - see
                       *TRIPLE* & *MORELLO_SUPPORT* in `Building Musl`)
    LTP_BUILD        - out-of-tree build path (created by LTP's build.sh script if needed)
    LTP_INSTALL      - destination path where LTP tests are to be installed
    TARGETS          - build targets, currently only pan, tools/apicmds and
                       testcases/kernel/syscalls are supported, it can be further narrowed
                       down for specific syscall testcases:
                       TARGETS="pan testcases/kernel/syscalls/${syscall}"
    TARGET_FEATURE   - morello+c64 (purecap) only: -march=morello+c64

.. code-block::

    CFLAGS="--target=${TRIPLE} ${TARGET_FEATURE} --sysroot=${MUSL} \
            -isystem ${KHDR_DIR}/usr/include -g -Wall"

    LDFLAGS="--target=${TRIPLE} -rtlib=compiler-rt --sysroot=${MUSL} \
             -fuse-ld=lld -static -L${LTP_BUILD}/lib"

    export CC=clang
    export HOST_CFLAGS="-O2 -Wall"
    export HOST_LDFLAGS="-Wall"
    export CONFIGURE_OPT_EXTRA="--prefix=/ --host=aarch64-linux-gnu --disable-metadata --without-numa"

    MAKE_OPTS="TST_NEWER_64_SYSCALL=no TST_COMPAT_16_SYSCALL=no" \
    TARGETS="pan tools/apicmds testcases/kernel/syscalls" BUILD_DIR="$LTP_BUILD" \
    CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" \
    ./build.sh -t cross -o out -ip "${LTP_INSTALL}"

This can be split into separate steps for ``configure``, ``build`` and ``install`` by
specifying additional parameter to ``build.sh`` detailing each step:

.. code-block::

        -r configure   - run configure only
        -r build       - run build only
        -r install     - run install only

Running tests:
--------------
.. code-block::

    runltp -f morello_transitional,morello_transitional_extended

Skipping tests:
---------------

In certain cases, you might want to skip tests that are known to be failing or are yet to be supported.

This is done by passing a file containing the names of the tests to be skip with the option ``-S`` to
``runltp``.

We currently provide the following skip files under ``runtest/``:
 - ``syscalls_morello_musl_skip``: Skips all tests failing on a regular AArch64 Musl based system.
 - ``syscalls_morello_skip``: Skips all tests not considered due to the Morello build setup,
   or the run environment.
 - ``syscalls_morello_purecap_skip``: Skips all tests not considered while testing with purecap binaries.

``runltp`` does not support passing multiple skip files, however this can be circumvented by using
process substitution.

To use all relevant skip files for plain AArch64:

.. code-block::

    ./runltp -f syscalls -S <( cat runtest/syscalls_morello{,_musl}_skip )

To use all relevant skip files for pure-capability:

.. code-block::

    ./runltp -f syscalls -S <( cat runtest/syscalls_morello{,_musl,_purecap}_skip )


These skip files are comprehensive only for morello release 1.6 and above.

Developers playground
---------------------

First things first:  read the docs - LTP rules/guidelines still apply (see section below).
Plus there are number of resources linked throughout this document; when in doubt.
If there is still something missing, `Morello LTP mailing list <https://op-lists.linaro.org/mailman3/lists/linux-morello-ltp.op-lists.linaro.org>`_
is the right place to go.

The above applies to all contributions and issue reports.
Pull requests should give place to appropriate patch submission on the mailing list.

Note on *next*
~~~~~~~~~~~~~~~~
While working on ``morello/next`` be mindful of the dependencies (see ``'Building'``
section). Some testcases might require newer version of either (or all) of them.
This should, preferably, be clearly stated for each testcase affected.


Original README / Further reading
---------------------------------
Original Linux Test Project `README <README-LTP.md>`_ file.
